import "package:test/test.dart";

import '../web/package/data/Cell.dart';
import '../web/package/data/EmptyCell.dart';
import '../web/package/data/Mountain.dart';
import '../web/package/data/WorldMap.dart';
import '../web/package/exception/InvalidActionException.dart';
import '../web/package/exception/InvalidDirectionException.dart';

import '../web/package/util/Direction.dart';
import './mock/MineMock.dart';

main(){

  generateWorldEmpty(nb_x, nb_y) {
    var worldLine = new List<Cell>();
    for(var y = 0; y < nb_x*nb_y; y++){
      worldLine.add(EmptyCell.empty());
    }
    return worldLine;
  }

  group("WorldMap", () {
    test("constructor base cell  == player cell", () {
      WorldMap worldMap = WorldMap((nb_x, nb_y)=>generateWorldEmpty(nb_x, nb_y));
      expect(worldMap.player.x, equals(worldMap.base.x));
      expect(worldMap.player.y, equals(worldMap.base.y));
    });
    test("doActionPlayerMine() is not mine cell", () {
      WorldMap worldMap = WorldMap((nb_x, nb_y)=>generateWorldEmpty(nb_x, nb_y));
      var x = worldMap.player.x;
      var y = worldMap.player.y;
      worldMap.getWorld()[y][x]= new EmptyCell(x, y);
      try{
        worldMap.doActionPlayerMine();
        expect(false, equals(true));
      }on InvalidActionException {
        expect(true, equals(true));
      }
    });
    test("doActionPlayerMine() is mine cell", () {
      WorldMap worldMap = WorldMap((nb_x, nb_y)=>generateWorldEmpty(nb_x, nb_y));
      var x = worldMap.player.x;
      var y = worldMap.player.y;
      MineMock mineMock = new MineMock(10,x,y);
      mineMock.minecount = 10;
      mineMock.isEmpty=false;
      worldMap.getWorld()[y][x]= mineMock;
      try{
        worldMap.doActionPlayerMine();
        expect(true, equals(true));
      }on InvalidActionException {
        expect(false, equals(true));
      }
    });
    test("doActionPlayerDischarge() is on base", () {
      WorldMap worldMap = WorldMap((nb_x, nb_y)=>generateWorldEmpty(nb_x, nb_y));
      try{
        worldMap.doActionPlayerDischarge();
        expect(true, equals(true));
      }on InvalidActionException {
        expect(false, equals(true));
      }
    });
    test("doActionPlayerDischarge() is on base", () {
      WorldMap worldMap = WorldMap((nb_x, nb_y)=>generateWorldEmpty(nb_x, nb_y));
      worldMap.player.x++;
      worldMap.player.y++;
      try{
        worldMap.doActionPlayerDischarge();
        expect(false, equals(true));
      }on InvalidActionException {
        expect(true, equals(true));
      }
    });

    test("doActionPlayerMove() invalid direction", () {
      WorldMap worldMap = WorldMap((nb_x, nb_y)=>generateWorldEmpty(nb_x, nb_y));
      try{
        worldMap.doActionPlayerMove(null);
        expect(false, equals(true));
      }on InvalidDirectionException {
        expect(true, equals(true));
      }
    });

    test("doActionPlayerMove() direction north limit of map", () {
      WorldMap worldMap = WorldMap((nb_x, nb_y)=>generateWorldEmpty(nb_x, nb_y));
      worldMap.player.y=0;
      try{
        worldMap.doActionPlayerMove(Direction.NORTH);
        expect(false, equals(true));
      }on InvalidDirectionException {
        expect(true, equals(true));
      }
    });

    test("doActionPlayerMove() direction north cell mountain", () {
      WorldMap worldMap = WorldMap((nb_x, nb_y)=>generateWorldEmpty(nb_x, nb_y));
      worldMap.player.y=1;
      worldMap.getWorld()[0][worldMap.player.x] = Mountain(0,worldMap.player.x);
      try{
        worldMap.doActionPlayerMove(Direction.NORTH);
        expect(false, equals(true));
      }on InvalidDirectionException {
        expect(true, equals(true));
      }
    });

    test("doActionPlayerMove() direction north cell mountain", () {
      WorldMap worldMap = WorldMap((nb_x, nb_y)=>generateWorldEmpty(nb_x, nb_y));
      worldMap.player.y=1;
      worldMap.getWorld()[0][worldMap.player.x] = EmptyCell(0,worldMap.player.x);
      worldMap.doActionPlayerMove(Direction.NORTH);
      expect(worldMap.player.y, equals(0));
    });

    test("doActionPlayerMove() direction south limit of map", () {
      WorldMap worldMap = WorldMap((nb_x, nb_y)=>generateWorldEmpty(nb_x, nb_y));
      worldMap.player.y=20-1;
      try{
        worldMap.doActionPlayerMove(Direction.SOUTH);
        expect(false, equals(true));
      }on InvalidDirectionException {
        expect(true, equals(true));
      }
    });

    test("doActionPlayerMove() direction south cell mountain", () {
      WorldMap worldMap = WorldMap((nb_x, nb_y)=>generateWorldEmpty(nb_x, nb_y));
      worldMap.player.y=20-2;
      worldMap.getWorld()[20-1][worldMap.player.x] = Mountain(0,worldMap.player.x);
      try{
        worldMap.doActionPlayerMove(Direction.SOUTH);
        expect(false, equals(true));
      }on InvalidDirectionException {
        expect(true, equals(true));
      }
    });

    test("doActionPlayerMove() direction south cell mountain", () {
      WorldMap worldMap = WorldMap((nb_x, nb_y)=>generateWorldEmpty(nb_x, nb_y));
      worldMap.player.y=20-2;
      worldMap.getWorld()[20-1][worldMap.player.x] = EmptyCell(0,worldMap.player.x);
      worldMap.doActionPlayerMove(Direction.SOUTH);
      expect(worldMap.player.y, equals(20-1));
    });


    test("doActionPlayerMove() direction west limit of map", () {
      WorldMap worldMap = WorldMap((nb_x, nb_y)=>generateWorldEmpty(nb_x, nb_y));
      worldMap.player.x=0;
      try{
        worldMap.doActionPlayerMove(Direction.WEST);
        expect(false, equals(true));
      }on InvalidDirectionException {
        expect(true, equals(true));
      }
    });


    test("doActionPlayerMove() direction west cell mountain", () {
      WorldMap worldMap = WorldMap((nb_x, nb_y)=>generateWorldEmpty(nb_x, nb_y));
      worldMap.player.x=1;
      worldMap.getWorld()[worldMap.player.y][0] = Mountain(worldMap.player.y,0);
      try{
        worldMap.doActionPlayerMove(Direction.WEST);
        expect(false, equals(true));
      }on InvalidDirectionException {
        expect(true, equals(true));
      }
    });

    test("doActionPlayerMove() direction west cell empty", () {
      WorldMap worldMap = WorldMap((nb_x, nb_y)=>generateWorldEmpty(nb_x, nb_y));
      worldMap.player.x=1;
      worldMap.getWorld()[worldMap.player.y][0] = EmptyCell(worldMap.player.y,0);
      worldMap.doActionPlayerMove(Direction.WEST);
      expect(worldMap.player.x, equals(0));
    });


    test("doActionPlayerMove() direction east limit of map", () {
      WorldMap worldMap = WorldMap((nb_x, nb_y)=>generateWorldEmpty(nb_x, nb_y));
      worldMap.player.x=20-1;
      try{
        worldMap.doActionPlayerMove(Direction.EAST);
        expect(false, equals(true));
      }on InvalidDirectionException {
        expect(true, equals(true));
      }
    });

    test("doActionPlayerMove() direction east cell mountain", () {
      WorldMap worldMap = WorldMap((nb_x, nb_y)=>generateWorldEmpty(nb_x, nb_y));
      worldMap.player.x=20-2;
      worldMap.getWorld()[worldMap.player.y][20-1] = Mountain(worldMap.player.y,0);
      try{
        worldMap.doActionPlayerMove(Direction.EAST);
        expect(false, equals(true));
      }on InvalidDirectionException {
        expect(true, equals(true));
      }
    });

    test("doActionPlayerMove() direction east cell empty", () {
      WorldMap worldMap = WorldMap((nb_x, nb_y)=>generateWorldEmpty(nb_x, nb_y));
      worldMap.player.x=20-2;
      worldMap.getWorld()[worldMap.player.y][20-1] = EmptyCell(worldMap.player.y,0);
      worldMap.doActionPlayerMove(Direction.EAST);
      expect(worldMap.player.x, equals(20-1));
    });


  });
}
