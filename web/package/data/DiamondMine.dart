import 'Mine.dart';
import '../util/Ore.dart';

/**
 * A diamond mine is a mine composed of diamond ores
 */
class DiamondMine extends Mine {

  /**
   * The image path of the diamond mine
   */
  static const _IMG_PATH = "images/diamond.png";

  /**
   * The name of the resource
   */
  static const _NAME = "diamond";

  /**
   * The resource type
   */
  static const _TYPE = Ore.DIAMOND;

  /**
   * The initial resource quantity in the mine
   */
  static const _INITIAL_RESOURCE_QUANTITY = 3;

  /**
   * The frequency for the mine to appear
   */
  static const _FREQUENCY = 4;

  /**
   * Class constructor
   */
  DiamondMine(int x, int y):super(_INITIAL_RESOURCE_QUANTITY,x,y);

  /**
   * Class constructor for a diamond mine without specific coordinates
   */
  DiamondMine.empty():super(_INITIAL_RESOURCE_QUANTITY,0,0);

  /**
   * Returns the image path of the mine
   */
  @override
  String getImgPath() {
    return _IMG_PATH;
  }

  /**
   * Returns the ore type of the diamond mine
   */
  Ore getOreType() {
    return _TYPE;
  }

  /**
   * Returns the number of resources that can be mined
   */
  int getMinedCount() {
    return 1;
  }

  /**
   * Returns the frequency for the mine to appear in the map
   */
  @override
  int getFrequency() {
    return _FREQUENCY;
  }

  /**
   * Returns the name of the ore resource
   */
  @override
  String getResourceName() {
    return _NAME;
  }

  /**
   * Creates a new diamond mine instance without specific coordinates
   */
  @override
  createNewInstance() => DiamondMine.empty();

}