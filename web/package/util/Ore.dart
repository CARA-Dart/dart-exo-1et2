import 'Enum.dart';

/**
 * The different types of ore in the game
 */
class Ore extends Enum<String>{

  const Ore(String value) : super(value);

  static const Ore COPPER = const Ore("Copper");
  static const Ore GOLD = const Ore("Gold");
  static const Ore SILVER = const Ore("Silver");
  static const Ore DIAMOND = const Ore("Diamond");
}