/**
 * The different game directions
 */
enum Direction{
  NORTH,
  SOUTH,
  EAST,
  WEST
}