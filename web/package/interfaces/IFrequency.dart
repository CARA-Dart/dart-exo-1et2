import '../data/Cell.dart';

/**
 * An interface allowing to get the apparition frequency for a cell and also to create a new instance of it
 */
abstract class IFrequency<T extends Cell> {
  int getFrequency();
  T createNewInstance();
}